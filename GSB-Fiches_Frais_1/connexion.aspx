﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="connexion.aspx.cs" Inherits="GSB_Fiches_Frais_1.connexion" %>
<%@ Import namespace="GSB_Fiches_Frais_1.mesClasses.outils" %> 
<%@ Import namespace="GSB_Fiches_Frais_1.mesClasses.metier_et_controle" %>
<%@ Assembly Name="MySql.Data" %> <!--dll dans le répertoire Bin du projet -->
<%@ Import Namespace="MySql.Data.MySqlClient" %> <!--Référence le namespace présent dans l'assembly ci-dessus pour uiliser les classes d'accès aux données -->
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>   
    <link href="includes/style.css" rel="stylesheet"/>
    <!-- jQuery -->
    <script src="lib/jquery/jquery.min.js"></script>
    <!-- JavaScript Boostrap plugin -->
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
    <%  string errorMessage = null;
        string successMessage = null; %>
        <div class="container">
    <%
        string formAction = Request.FilePath;

        if(Request.Form["btnCnx"] != null)
        {
            string username = Request.Form["username"];
            string pwd = Request.Form["pwd"];
            //cast de object en Cvisiteurs
            Cvisiteurs ovisiteurs = (Cvisiteurs)Application["ovisiteurs"];
            Dictionary<string, Cvisiteur> ocoll = ovisiteurs.Ocoll_key_log_mdp;
            try
            {
                Cvisiteur ovisiteur = ocoll[username + pwd];
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
        }

    %>
             <br/>
             <br/>
                
              <form class="form-horizontal formLogin" role="form" method="post" action="<%=formAction%>""> <!-- $formAction est dans la page php qui inclut ce fichier-->
                                      
                        <div class="form-group">
                          <label class="control-label col-sm-4 text-primary">Username</label>
                          <div class="col-sm-4">
                          <input type="text" class="form-control " name="username" placeholder="Enter username" required="required" autofocus="autofocus"/>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-4 text-primary">Password</label>
                          <div class="col-sm-4">
                          <input type="password" class="form-control" name="pwd" placeholder="Enter password" required="required"/>
                          </div>
                        </div>
                        <div class="col-sm-offset-4 col-sm-8">
                          <button type="submit" name="btnCnx" class="btn btn-default">Submit</button>
                        </div>
              
              </form>
            <br />
            <br />
            <br />
              <% if (successMessage != null){%>
                <div class="alert alert-success container"><%= successMessage %></div>
              <%}%>
              <% if (errorMessage != null){%>
                    <div class="alert alert-danger container"><%= errorMessage %></div>
              <%}%>
        </div>
</body>
</html>
