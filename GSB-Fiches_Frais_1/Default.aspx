﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GSB_Fiches_Frais_1.Default" %>
<%@ Import namespace="GSB_Fiches_Frais_1.mesClasses.outils" %> 
<%@ Import namespace="GSB_Fiches_Frais_1.mesClasses.metier_et_controle" %>
<%@ Assembly Name="MySql.Data" %> <!--dll dans le répertoire Bin du projet -->
<%@ Import Namespace="MySql.Data.MySqlClient" %> <!--Référence le namespace présent dans l'assembly ci-dessus pour uiliser les classes d'accès aux données -->
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>   
    <link href="includes/style.css" rel="stylesheet"/>
    <!-- jQuery -->
    <script src="lib/jquery/jquery.min.js"></script>
    <!-- JavaScript Boostrap plugin -->
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
    <% 
        //initialisation des variables d'avertissement
        string successMessage = null;
        string errorMessage = null;

       
            //récupération de la classe de contrôle à partir de la variable de session 
         CligneFraisHorsForfaits olignes = (CligneFraisHorsForfaits)Application["olignes"];  // utiliser aussi plus loin dans le code
         CficheFraiss oficheFraiss = (CficheFraiss)Application["oficheFraiss"];
       



        if(Request.Form["btnFHF"] != null)
        {
            string testbtn = Request.Form["btnFHF"];
            string montant = Request.Form["montant"]; //sera parser en décimal par la classe de contrôle
            string libelle = Request.Form["libelle"];

            // Le test se fait avec le visiteur id = 131
            string idvisiteur = Convert.ToString(Session["idVisiteur"]);

            //verification de la fiche de frais pour le mois annee en cours 
            Dictionary<string,CficheFrais> ocollFF = oficheFraiss.ocollFicheFrais_KeyId;
            CficheFrais oficheFrais = null;
            bool ffExist = ocollFF.TryGetValue(idvisiteur + CtraitementDate.getAnneeMoisEnCours(), out oficheFrais); //ffExist FicheFrais existe ?
            if(!ffExist)
            {
                try
                {
                    //je rajoute la fiche de frais si elle n'existe pas dans la base puis dans la collection
                    oficheFraiss.ajouterFicheFrais(idvisiteur, CtraitementDate.getAnneeMoisEnCours(), "CR");
                }
                catch(MySqlException ex)
                {
                    string message = ex.Message;


                }

            }
            try
            {
                olignes.ajouterLigneFHF(idvisiteur, libelle, montant); //remontée d'exception de la classe outils

                successMessage = "Frais hors forfait correctement enregistrés. Merci";
            }
            catch(MySqlException ex)
            {
                errorMessage = ex.Message;
            }
        }


        int id_lhf = Convert.ToInt32(Request.QueryString["id_lhf"]);


        if (id_lhf != 0) //Rien a recuperer dans l'URL
        {
            //Dictionary<int, CligneFraisHorsForfait> ocollFHF_KeyId = olignes.ocollFicheFHF_KeyId;
            string result = olignes.effacerLigneHF(id_lhf);
            if(result != null)
            {
                errorMessage = "Erreur au niveau couche de données : " + result;
            }
        }
    %>
    <div class="container">
    <%string formAction = Request.FilePath; %> <!--Donne le chemin vers le fichier aspx en cours - equivalent $_SERVER["PHP_SELF] en PHP-->
    <h3><p class="text-primary page-header">Saisie des frais hors forfait<span class="text-primary glyphicon glyphicon-pencil" /></p></h3>
    <br/>      
    <form id="formFHF" method="post" action="<%= formAction %>" class="form-horizontal" role="form"> 
           <!--   <input type="hidden" name="leformulaire" value="validerSaisieFHF" /> -->
            <div class="form-group">
              <label class="control-label col-sm-2" for="libelle">Libellé:</label>
              <div class="col-sm-10">
                  <textarea class="form-control" name="libelle" placeholder="Entrer libelle frais" required="required" autofocus="autofocus"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="montant">Montant:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="montant" placeholder="Entrer montant frais" required="required"/>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="btnFHF" class="btn btn-default">Enregistrer</button>
              </div>
            </div>
    </form>

    <h4><p class="text-primary">Récapitulatif des frais hors forfait du mois : <%Response.Write(CtraitementDate.getMoisEnLettre(System.DateTime.Now.Month) +  " ");%><span class="glyphicon glyphicon-align-justify"/></p></h4>
    <br/>
    <table class='table table-hover'>
    <thead>
       <tr class='bg-info'>
        <th>Libellé</th>
        <th>Date</th>
        <th>Montant</th>
        <th>Action</th>  <!-- supprimer ligne -->
      </tr>
    </thead>
    <tbody>
        
        <%  
            string idvisiteur_ = Convert.ToString(Session["idVisiteur"]);
            string mois = Convert.ToString(Session["anneeMois"]);
            Dictionary<int,CligneFraisHorsForfait> ocollFicheFHF = olignes.getCollLhfByVisiteurAndMois(idvisiteur_,mois);
            if(ocollFicheFHF != null)
            { 
                foreach(CligneFraisHorsForfait oligne in ocollFicheFHF.Values) //parcours des valeurs du dictionnaire 
                { 
            
                 %>

                            <tr>
                                <td><%= oligne.libelle %></td>
                                <td><%= oligne.date.ToShortDateString() %></td>
                                 <!--devient rouge si montant > 100 :-->
                                <td <%=(oligne.montant > 100)?"class='text-danger'":""%>><%= oligne.montant %></td>
                                <td> 
                                    <button type="button" class="btn btn-danger btn-sm" title="Supprimer" data-toggle="modal" data-target="#<%= oligne.id %>">
                                    Supprimer
                                    </button>
                                    <div class="modal fade" id="<%= oligne.id %>" role="dialog">
                                        <div class="modal-dialog modal-sm">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Avertissement</h4>
                                            </div>
                                            <div class="modal-body">
                                              <p>Voulez vous vraiment supprimer l'enregistrement ?</p>
                                            </div>
                                            <div class="modal-footer">
                                              <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                                              <a href="<%="Default.aspx?id_lhf="%><%= oligne.id %>" class="btn btn-danger btn-sm">OUI</a>
                                              <a href="#" data-dismiss="modal" class="btn btn-primary btn-sm">NON</a> <!--data-dismiss="modal" permet de fermer toutes modals actives -->
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                            
                                </td> <!--id_lhf pour id ligne hors forfait -->
                            
                            </tr>
            <% } //fin foreach
           } //fin if
           else
            { errorMessage = errorMessage + " Il n'y a pas d'enregistrement de frais hors forfait pour le mois considéré." ; }
           %>
                   
    </tbody>
  </table>
        <% if (successMessage != null){%>
            <div class="alert alert-success container"><%= successMessage %></div>
        <%}%>
        <% if (errorMessage != null){%>
            <div class="alert alert-danger container"><%= errorMessage %></div>
        <%}%>
 </div>
</body>
</html>
