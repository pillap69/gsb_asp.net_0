﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using GSB_Fiches_Frais_1.mesClasses.outils;
using System.Globalization;

namespace GSB_Fiches_Frais_1.mesClasses.metier_et_controle
{

    /* -----------------Classe métier et contrôle concerant les ligne de frais hors forfait -------------------------*/
    public class CligneFraisHorsForfait
    {
        public int id;
        public DateTime date;
        public string idVisiteur;
        public string libelle;
        public string mois;
        public decimal montant;


        

        public CligneFraisHorsForfait(int sid,DateTime sdate,string sidVisiteur,string slibelle,string smois, decimal smontant)
        {
          
            date = sdate;
            idVisiteur = sidVisiteur;
            libelle = slibelle;
            mois = smois;
            montant = smontant;
        }
    }

    public class CligneFraisHorsForfaits
    {
        private List<CligneFraisHorsForfait> _ocollFicheFraisHorsForfait = new List<CligneFraisHorsForfait>();
        private Dictionary<int, CligneFraisHorsForfait> _ocollFicheFHF_KeyId = new Dictionary<int, CligneFraisHorsForfait>();

        public Dictionary<int, CligneFraisHorsForfait> ocollFicheFHF_KeyId
        {
            get { return _ocollFicheFHF_KeyId; }
            
        }

        public List<CligneFraisHorsForfait> ocollFicheFraisHorsForfait
        {
            get { return _ocollFicheFraisHorsForfait; }
            
        }

        public CligneFraisHorsForfaits()
        {

            Cdao odao = new Cdao();
            string query = "select * from ligneFraisHorsForfait";
            
            MySqlDataReader ord = odao.getReader(query);
   

            //parcours des enregistrements
            while(ord.Read())
            {
                CligneFraisHorsForfait oligne = new CligneFraisHorsForfait(Convert.ToInt32(ord["id"]),
                   Convert.ToDateTime(ord["date"]),Convert.ToString(ord["idVisiteur"]), Convert.ToString(ord["libelle"]),
                   Convert.ToString(ord["mois"]), Convert.ToDecimal(ord["montant"]));
                
                _ocollFicheFraisHorsForfait.Add(oligne);
                _ocollFicheFHF_KeyId[oligne.id] = oligne;

            }
            /*ord.Close();
            ocnx.Close(); */  
            /* inutible de fermer car l'objet odao est detruit à la fin du conctructeur 
             * de CligneFraisHorsForfait et donc les objets liés par composition aussi  
             c'est à dire ord et ocnx */    
        }

        public string effacerLigneHF(int sid)
        {
            Cdao odao = new Cdao();
            string query = "delete from lignefraishorsforfait  where lignefraishorsforfait.id = " + Convert.ToString(sid);
            string result = odao.deleteEnreg(query);
            if (result == null) //sil'opération delete s'est effectuee avec succes
            {
                if (_ocollFicheFHF_KeyId.ContainsKey(sid))
                {
                    _ocollFicheFHF_KeyId.Remove(sid);
                    return null;
                }
            }
            return result;

        }

        public  void ajouterLigneFHF(string sidVisiteur,string slibelle,string smontant)
        {
            // Verifie que c'est une valeur decimale
            decimal montant ;
            bool conversion = decimal.TryParse(smontant,out montant); //verifie si le montant est un nombre
            //if(!conversion)
            //{
                //return false;

            //}
            montant = Math.Round(montant, 2);
            Cdao odao = new Cdao();
           
            //regle le probleme des datetime avec mysql
            DateTime dateEnFrancais = CtraitementDate.getDateCourante();
            string dateMySql = dateEnFrancais.ToString("yyyy-MM-dd");
            //string dateEnAnglais = DateTime.Parse(dateEnFrancais, anglais).ToShortDateString();
            string query = "INSERT INTO lignefraishorsforfait(idVisiteur,mois,libelle,date,montant) VALUES ('" +
                sidVisiteur + "'," + "'" + Convert.ToString(CtraitementDate.getAnneeMoisEnCours()) + "'," + 
                "'" + slibelle + "'," + "'" + dateMySql + "'," + smontant + ")" ;

            odao.insertEnreg(query);


            //recup dans la base de l'id de l'insert ci-dessus
            object max = odao.recupMaxChampTable("id", "lignefraishorsforfait");
            CligneFraisHorsForfait oligne = new CligneFraisHorsForfait(Convert.ToInt32(max),
            Convert.ToDateTime(CtraitementDate.getDateCourante()), Convert.ToString(sidVisiteur), Convert.ToString(slibelle),
            Convert.ToString(CtraitementDate.getAnneeMoisEnCours()), Convert.ToDecimal(smontant));

            _ocollFicheFraisHorsForfait.Add(oligne);
            _ocollFicheFHF_KeyId[oligne.id] = oligne;
        }

        public Dictionary<int,CligneFraisHorsForfait> getCollLhfByVisiteur(string sidVisiteur)
        {
            Dictionary<int, CligneFraisHorsForfait> _oCollLhfUnVisiteur_KeyId = new Dictionary<int, CligneFraisHorsForfait>();
            foreach(CligneFraisHorsForfait oligne in _ocollFicheFHF_KeyId.Values)
            {
                if(oligne.idVisiteur ==  sidVisiteur)
                {
                    _oCollLhfUnVisiteur_KeyId.Add(oligne.id, oligne);

                }

            }

            if (_oCollLhfUnVisiteur_KeyId.Count != 0)
            {
                return _oCollLhfUnVisiteur_KeyId;
            }

            return null;

        }

        public Dictionary<int, CligneFraisHorsForfait> getCollLhfByVisiteurAndMois(string sidVisiteur, string smois)
        {
            Dictionary<int, CligneFraisHorsForfait> _oCollLhfUnVisiteur_KeyId = new Dictionary<int, CligneFraisHorsForfait>();
            // Recherche dans la collection de valeurs (values) du dictionnaire
            foreach (CligneFraisHorsForfait oligne in _ocollFicheFHF_KeyId.Values)
            {
                if (oligne.idVisiteur == sidVisiteur && oligne.mois == smois)
                {
                    _oCollLhfUnVisiteur_KeyId.Add(oligne.id, oligne);

                }

            }

            // Si la collection à un nombre d'éléments supérieur à 0

            if (_oCollLhfUnVisiteur_KeyId.Count != 0)
            {
                return _oCollLhfUnVisiteur_KeyId;
            }

            return null;

        }

    }

    /*--------------------------------Classes métier et contrôle concernant les ficheFrais-----------------------------------------*/

    public class CficheFrais
    {
        public string idVisiteur;
        public string mois;
        //le après le type ? permet de rendre les STRUCT nullable
        public int? nbJustificatifs;
        public Decimal? montantValide;
        public DateTime? date;
        public string idEtat; //reference fraiForfait

        public CficheFrais(string sidVisiteur,string smois,int? snbJustif,Decimal? smtValide,DateTime? sdate,string sidEtat)
        {
            idVisiteur = sidVisiteur;
            mois = smois;
            nbJustificatifs = snbJustif;
            montantValide = smtValide;
            date = sdate;
            idEtat = sidEtat;
        }

    }

    public class CficheFraiss
    {
        private List<CficheFrais> _ocollFicheFrais = new List<CficheFrais>();
        private Dictionary<string, CficheFrais> _ocollFicheFrais_KeyId = new Dictionary<string, CficheFrais>();

        public Dictionary<string, CficheFrais> ocollFicheFrais_KeyId
        {
            get { return _ocollFicheFrais_KeyId; }
            
        }

        public List<CficheFrais> ocollFicheFrais
        {
            get { return _ocollFicheFrais; }
            
        }

        public CficheFraiss()
        {


            //utilisation d'une classe outil Cdao
            Cdao odao = new Cdao();
            string query = "select * from ficheFrais";
            MySqlDataReader ord = odao.getReader(query);

            //parcours des enregistrements
            while (ord.Read())
            {
                CficheFrais oficheFrais = null;
                // En cas de valeur null dans la base pour les champs considérés (cas des fiches non traités comptablement)
                if (ord["nbJustificatifs"] is DBNull && ord["montantValide"] is DBNull && ord["dateModif"] is DBNull)
                {
                    oficheFrais = new CficheFrais(Convert.ToString(ord["idVisiteur"]),
                    Convert.ToString(ord["mois"]), 0, 0, null, Convert.ToString(ord["idEtat"]));

                }
                else
                {
                    oficheFrais = new CficheFrais(Convert.ToString(ord["idVisiteur"]),
                       Convert.ToString(ord["mois"]), Convert.ToInt16(ord["nbJustificatifs"]),Convert.ToDecimal(ord["montantValide"]),
                       Convert.ToDateTime(ord["dateModif"]), Convert.ToString(ord["idEtat"]));
                }

                _ocollFicheFrais.Add(oficheFrais);
                _ocollFicheFrais_KeyId[oficheFrais.idVisiteur + oficheFrais.mois] = oficheFrais;

            }
           
        }

        // Autres param en optionnel et si utilisé alors convert dans le vrai type dans la fonction
        public void ajouterFicheFrais(string sidVisiteur,string smois,string sidEtat,params string[] stabParamOptionnel)
        {
            
            Cdao odao = new Cdao();
            string query = string.Format("insert into ficheFrais(idVisiteur,mois,idEtat) values ('{0}','{1}','{2}')", sidVisiteur, smois, sidEtat);
            odao.insertEnreg(query);
           
            //ajout ficheFrais dans les collections
            CficheFrais oficheFrais = new CficheFrais(sidVisiteur,smois, 0, 0, null,sidEtat);
            _ocollFicheFrais.Add(oficheFrais);
            _ocollFicheFrais_KeyId.Add(sidVisiteur + smois, oficheFrais);
            
           
            
          
        }


    }

    /*--------------------------------Classes métier et contrôle concernant les visiteurs--------------------------------------*/

    public class Cvisiteur
    {
        public string id;
        public string nom;
        public string prenom;
        public string login;
        public string mdp;
        public string adresse = null;
        public string cp = null;
        public string ville =null;

        public Cvisiteur(string sid,string snom,string sprenom,string slogin, string smdp)
        {
            id = sid;
            nom = snom;
            prenom = sprenom;
            login = slogin;
            mdp = smdp;
        }

    }

    public class Cvisiteurs
    {
        // clef login + mdp
        private Dictionary<string, Cvisiteur> _ocoll_key_log_mdp = new Dictionary<string, Cvisiteur>();

        public Dictionary<string, Cvisiteur> Ocoll_key_log_mdp
        {
            get
            {
                return _ocoll_key_log_mdp;
            }

            set
            {
                _ocoll_key_log_mdp = value;
            }
        }
        public Cvisiteurs()
        {
            Cdao odao = new Cdao();
            string query = "select * from visiteur";
            MySqlDataReader ord = odao.getReader(query);

            while(ord.Read())
            {
                Cvisiteur ovisiteur = new Cvisiteur(Convert.ToString(ord["id"]),Convert.ToString(ord["nom"]),
                    Convert.ToString(ord["prenom"]),Convert.ToString(ord["login"]),Convert.ToString(ord["mdp"]));
                _ocoll_key_log_mdp.Add(Convert.ToString(ord["login"]) + Convert.ToString(ord["mdp"]), ovisiteur);

            }
        }

  
    }
    }
     