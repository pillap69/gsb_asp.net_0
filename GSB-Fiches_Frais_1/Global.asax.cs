﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using GSB_Fiches_Frais_1.mesClasses.metier_et_controle;
using GSB_Fiches_Frais_1.mesClasses.outils;


namespace GSB_Fiches_Frais_1
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            Application["olignes"] = new CligneFraisHorsForfaits();
            Application["oficheFraiss"] = new CficheFraiss();
            Application["ovisiteurs"] = new Cvisiteurs();
            
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["olignes"] = new CligneFraisHorsForfaits();
            Session["oficheFraiss"] = new CficheFraiss();

            Session["idVisiteur"] = "a17";
            Session["anneeMois"] = CtraitementDate.getAnneeMoisEnCours();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}